(ns website-client.handler
  (:require [clj-http.client :as client]
            [clojure.pprint :as pp]
            [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [hiccup.page :refer [include-js include-css html5]]
            [website-client.middleware :refer [wrap-middleware]]
            [config.core :refer [env]]))

(def mount-target
  [:div#app
      [:h3 "ClojureScript has not been compiled!"]
      [:p "please run "
       [:b "lein figwheel"]
       " in order to start the compiler"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css (if (env :dev)  "https://fonts.googleapis.com/icon?family=Material+Icons" "https://fonts.googleapis.com/icon?family=Material+Icons")
                (if (env :dev) "https://code.getmdl.io/1.3.0/material.indigo-pink.min.css" "https://code.getmdl.io/1.3.0/material.indigo-pink.min.css")
                (if (env :dev) "/css/site.css" "/css/site.min.css"))])
                   

(defn loading-page []
  (html5
    (head)
    [:body {:class "body-container"}
     mount-target
     (include-js "/js/app.js" "https://code.getmdl.io/1.3.0/material.min.js")]))



(defn api-user-proxy [request]
  (let [request-method (get-in request [:request-method])
        body (get-in request [:body])
        query-string (get-in request [:query-string])
        server-name (get-in request [:server-name])
        uri (get-in request [:uri])
        server-port (get-in request [:server-port])
        scheme (get-in request [:scheme])]
    
    (if (= request-method :get)
      (pp/pprint "It's a GET!")
        (client/get (str scheme "://" server-name ":8080" uri))
        )
    )
  )
  
  

(defroutes routes
  ;; Website routes
  (GET "/" [] (loading-page))
  (GET "/login" [] (loading-page))

  (GET "/api/user" request (api-user-proxy request))
  
  ;; Static files
  (resources "/")

  ;; Default
  (not-found "Not Found"))

(def app (wrap-middleware #'routes))
