(ns website-client.core
    (:require [reagent.core :as reagent]
              [secretary.core :as secretary :include-macros true]
              [accountant.core :as accountant]
              [ajax.core :refer [GET POST PUT DELETE]]
              ))

(declare
 append-child
 create-user
 create-user-input
 close-dialog
 delete-user-by-id
 user-edit
 user-in-focus
 handle-delete-user-by-id
 handle-update-user-by-id
 handle-new-user
 handle-login
 get-create-user-input
 get-edit-user-input
 get-el-by-id
 get-users
 get-user-from-item
 get-users-from-collection
 testa
 testb
 table
 api-get
 update-user-by-id
 show-dialog
 current-users
 get-current-users
 login
 logout
 users-table
 users-table-body)

(def host "localhost:8080")

;; -------------------------
;; Views
(defn users-table []
  (let [users (get-in @current-users [:users])]
   [:table.mdl-data-table.mdl-js-data-table.mdl-data-table.full-width.center-margin
    [:thead
     [:tr
      [:th {:hidden "hidden"} "User Id"]
      [:th.mdl-data-table__cell--non-numeric "Email"]
      [:th.mdl-data-table__cell--non-numeric "Name"]
      [:th.mdl-data-table__cell--non-numeric "Subscription"]
      [:th.mdl-data-table__cell--non-numeric "Creation Date"]
      [:th.mdl-data-table__cell--non-numeric "Exceptions"]
      [:th.mdl-data-table__cell--non-numeric "Edit"]
      [:th.mdl-data-table__cell--non-numeric "Delete"]
      ]]
    [:tbody
     (for [u users] ^{:key u}
       [:tr {:id (u :id) }
        [:td {:hidden "hidden"} (u :id)]
        [:td.mdl-data-table__cell--non-numeric (get-in u [:email])]
        [:td.mdl-data-table__cell--non-numeric (str (u :lastName) " " (u :firstName))]
        [:td.mdl-data-table__cell--non-numeric "Subscription Type"]
        [:td.mdl-data-table__cell--non-numeric "Date Added"]
        [:td.mdl-data-table__cell--non-numeric "Exception Count"]
        [:td.mdl-data-table__cell--non-numeric
         [:button.mdl-button.mdl-js-button.mdl-button.mdl-button--accent.mdl-button--icon {:on-click #(show-dialog "edit-user-dialog" (u :id))} [:i.material-icons "create"]]]
        [:td.mdl-data-table__cell--non-numeric
         [:button.mdl-button.mdl-js-button.mdl-button.mdl-button--accent.mdl-button--icon {:on-click #(delete-user-by-id (u :id))} [:i.material-icons "delete"]]]
        ])]
    ]))

(defn user-create []
  [:div
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#first-name.mdl-textfield__input {:type "First Name"}]
     [:label.mdl-textfield__label {:for "First Name"} "First Name"]]]
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#last-name.mdl-textfield__input {:type "Last Name"}]
     [:label.mdl-textfield__label {:for "Last Name"} "Last Name"]]]
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#email.mdl-textfield__input {:type "Email"}]
     [:label.mdl-textfield__label {:for "Email"} "Email"]]]
      [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#password.mdl-textfield__input {:type "Password"}]
     [:label.mdl-textfield__label {:for "Password"} "Password"]]]
   [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-free"}
     [:input#subscription-type-free.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Free"]]]
      [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-auto-cart"}
     [:input#subscription-type-auto-cart.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Automated Cart"]]]
      [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-post-gen"}
     [:input#subscription-type-post-gen.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Social Media Post Generation"]]]
   ])

(defn user-edit []
  [:div
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#edit-first-name.mdl-textfield__input {:type "First Name"}]
     [:label.mdl-textfield__label {:for "First Name"} "First Name"]]]
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#edit-last-name.mdl-textfield__input {:type "Last Name"}]
     [:label.mdl-textfield__label {:for "Last Name"} "Last Name"]]]
   [:form
    {:action "#"}
    [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
     [:input#edit-email.mdl-textfield__input {:type "Email"}]
     [:label.mdl-textfield__label {:for "Email"} "Email"]]]
   [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-free"}
     [:input#edit-subscription-type-free.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Free"]]]
   [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-auto-cart"}
     [:input#edit-subscription-type-auto-cart.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Automated Cart"]]]
   [:form
    [:label.mdl-checkbox.mdl-js-checkbox.mdl-js-ripple-effect
     {:for "subscription-type-post-gen"}
     [:input#edit-subscription-type-post-gen.mdl-checkbox__input
      {:type "checkbox"}]
     [:span.mdl-checkbox__label "Social Media Post Generation"]]]
   ])

(defn view-login []
  [:div
   [:h2 "DropHappy Login"]
   [:div.mdl-grid.center-items
    [:div.mdl-cell.mdl-cell--6-col 
     [:div.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label
      [:input#login-username.mdl-textfield__input {:type "text"}]
      [:label.mdl-textfield__label {:for "login-username"} "Username"]]]]
   [:div.mdl-grid.center-items
    [:div.mdl-cell.mdl-cell--6-col 
     [:div.mdl-textfield.mdl-js-textfield.mdl-textfield-floating-lable
      [:input#login-password.mdl-textfield__input {:type "password"}]
      [:label.mdl-textfield__label {:for "login-username"} "Password"]]]]
   [:div.mdl-grid.center-items
    [:div.mdl-cell.mdl-cell-6-col
     [:button.mdl-button.mdl-js-button.mdl-button.mdl-button--accent
      {:on-click #(login  (.-value (get-el-by-id "login-username")) (.-value (get-el-by-id "login-password")))} "Login"]
     [:button.mdl-button.mdl-js-button.mdl-button.mdl-button--accent
      {:on-click #(logout)}  "Logout"]
     ]]
   ])

(defn user-page []
  [:div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header
   [:header.mdl-layout__header
    [:div.mdl-layout__header-row
     [:span.mdl-layout-title "DropHappy"]     
     [:div.mdl-layout-spacer]
     [:nav.mdl-navigation.mdl-layout--large-screen-only
      [:a.mdl-navigation__link {:href ""} "User Management"]
      [:a.mdl-navigation__link {:href ""} "Subscriptions"]
      [:a.mdl-navigation__link {:href ""} "Exceptions"]
      [:a.mdl-navigation__link {:href ""} "Email"]]]]
   [:div.mdl-layout__drawer
    [:span.mdl-layout-title "DropHappy"]
    [:nav.mdl-navigation
     [:a.mdl-navigation__link {:href "/login"} "User Management"]
     [:a.mdl-navigation__link {:href ""} "Subscriptions"]
     [:a.mdl-navigation__link {:href ""} "Exceptions"]
     [:a.mdl-navigation__link {:href ""} "Email"]]]
   [:main.mdl-layout__content
    [:button.mdl-button.mdl-js-button.mdl-button--raised.mdl-button--colored.mdl-js-ripple-effect {:type "button" :on-click #(show-dialog "create-user-dialog")} "Create User"]
    [users-table]
    [:dialog#create-user-dialog.mdl-dialog
     [:h3.mdl-dialog__title "Create User"]
     [:div.mdl-dialog__content
      [user-create]]      
     [:div.mdl-dialog__actions
      [:button.mdl-button {:type "button" :on-click #(close-dialog "create-user-dialog")} "Close"]
      [:button.mdl-button
       {:type "button" :on-click #(create-user)}
       "Create User"]]]
    [:dialog#edit-user-dialog.mdl-dialog
     [:h3.mdl-dialog__title "Edit User"]
     [:div.mdl-dialog__content
      [user-edit]]      
     [:div.mdl-dialog__actions
      [:button.mdl-button {:type "button" :on-click #(close-dialog "edit-user-dialog")} "Close"]
      [:button.mdl-button
       { :type "button" :on-click #(update-user-by-id  (get-in @user-in-focus [:user-id]) (get-edit-user-input))}
       "Update User"]]]
    ]])

;; -------------------------
;; Reagent Atoms
(def current-users (reagent/atom {:users [{:firstName "Cody" :lastName "Helbling"}]}))

(def user-in-focus (reagent/atom {:user-id ""}))

;; -------------------------
;; Services  
(defn get-create-user-input []
  (prn "get-create-user-input")
  (let [email (.-value (get-el-by-id "email"))
        first-name (.-value (get-el-by-id "first-name"))
        last-name (.-value (get-el-by-id "last-name"))
        password (.-value (get-el-by-id "password"))]
    (prn email  first-name  last-name  password)
    {:firstName first-name
     :lastName last-name
     :email email
     :password password}
    ))

(defn get-edit-user-input []
  (prn "get-edit-user-input")
  (let [email (.-value (get-el-by-id "edit-email"))
        first-name (.-value (get-el-by-id "edit-first-name"))
        last-name (.-value (get-el-by-id "edit-last-name"))]
    (prn email  first-name  last-name)
    (let [m
          {:firstName first-name
           :lastName last-name
           :email email}]
      (select-keys m (for [[k v] m :when (not= v "")] k))
      )
    ))

;; -------------------------
;; Deconstruct Api Response
;; This could use a re-write.
(defn get-user-from-item [item]
  "Get 'item' from inside api response."
  ;; (prn "Item: " item)
  (apply merge (map #(hash-map (keyword (% :name)) (% :value)) item))
  )

(defn get-users-from-collection [collection]
  "Get a map of users from a 'collection' api response."
  ;; (prn "Collection: " collection)
  (map #(get-user-from-item (% :data)) (collection :items))
  )

;; -------------------------
;; Api Response Handlers
(defn handle-new-user [response]
    (let [collection (:collection (js->clj (.parse js/JSON response) :keywordize-keys true))
        users (get-users-from-collection collection)
        ]
      (swap! current-users assoc :users users)
      ))

(defn handle-create-user [response]
  (prn "response: " response)
  (let [collection (:collection (js->clj (.parse js/JSON response) :keywordize-keys true))
        users (get-users-from-collection collection)
        ]
    (prn "get-users-from-collection: " (get-users-from-collection collection))
    (prn "users: " type users)
    (get-users)
    ))

  (defn handle-delete-user-by-id [response]
    (prn "response: " response)
    (let [collection (:collection (js->clj (.parse js/JSON response) :keywordize-keys true))
          users (get-users-from-collection collection)
          ]
      (prn "get-users-from-collection: " (get-users-from-collection collection))
      (prn "users: " type users)
      (get-users)
      ))

  (defn handle-update-user-by-id [response]
    (prn "handle-update-user-by-id")
    (let [collection (:collection (js->clj (.parse js/JSON response) :keywordize-keys true))]
      (get-users)
      ))

  (defn handle-login [response]
    (prn "handle-login")
    (let [token (get-in (js->clj  response :keywordize-keys true) ["token"])]
      (.setItem (.-localStorage js/window) "token" (str "Token " token))
      ;; (prn "TEST: " (.getItem (.-localStorage js/window) "token"))
      ))

;; -------------------------
;; Api Calls
(defn get-users []
  (let [response (GET (str "http://" host "/api/user") {
                                                        :handler handle-new-user
                                                        :headers {:Authorization (.getItem (.-localStorage js/window) "token")}})]))

(defn create-user []
  (let [response (POST (str "http://" host "/api/user") {:method :post
                                                         :params (get-create-user-input)
                                                         :format :json 
                                                         :handler handle-create-user})]))

(defn delete-user-by-id [user-id]
  (let [response (DELETE (str "http://" host "/api/user") {:method :delete
                                                           :params {:id user-id}
                                                           :format :json 
                                                           :handler handle-delete-user-by-id})]))

(defn update-user-by-id [user-id updates]
  (let [response (PUT (str "http://" host "/api/user/" user-id) {:method :put
                                                           :params updates
                                                           :format :json 
                                                           :handler handle-update-user-by-id})]))
(defn login [username password]
  (let [response (POST (str "http://" host "/api/login")
                       {:method :post
                        :params {:username username :password password}
                        :format :json
                        :handler handle-login})]))

(defn logout []
  (remove-item! "token"))
  

;; -------------------------
;; Routes

(def page (atom #'view-login))

(defn current-page []
  [:div [@page]])

(secretary/defroute "/login" []
  (reset! page #'view-login))

(secretary/defroute "/" []
  (get-users)
  (reset! page #'user-page))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!)
  (mount-root))

;; -------------------------
;; DOM Manipulation Helpers
(defn get-el-by-id [id]
  (-> js/document
      (.getElementById id)))

(defn show-dialog
  ([html-id user-id]
   (prn (str "showing modal for html-id: " html-id " user-id: " user-id))
   (swap! user-in-focus assoc :user-id user-id)
   (prn (get-el-by-id html-id))
   (.showModal (get-el-by-id html-id)))
   ([html-id]
    (.showModal(get-el-by-id html-id))))

(defn close-dialog [id]
  (prn "closing dialog")
  (prn (get-el-by-id id))
  (.close (get-el-by-id id)))

(defn append-child [child parent]
  (.appendChild js/document child parent))

(defn set-item!
  "Set `key' in browser's localStorage to `val`."
  [key val]
  (.setItem (.-localStorage js/window) key val))

(defn get-item
  "Returns value of `key' from browser's localStorage."
  [key]
  (.getItem (.-localStorage js/window) key))

(defn remove-item!
  "Remove the browser's localStorage value for the given `key`"
  [key]
  (.removeItem (.-localStorage js/window) key))
