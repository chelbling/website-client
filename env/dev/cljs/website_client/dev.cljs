(ns ^:figwheel-no-load website-client.dev
  (:require
    [website-client.core :as core]
    [devtools.core :as devtools]))

(devtools/install!)

(enable-console-print!)

(core/init!)
